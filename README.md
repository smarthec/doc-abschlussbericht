# Abschlussbericht für das Projekt Smart_HEC

Wir veröffentlichen an dieser Stelle den wissenschaftlichen Abschlussbericht des Projekts "Smart_HEC".

**Vorhabentitel:** „Der HeizCheck für alle mit KI – Empowerment durch eine smarte Bewertung der Heizkostenabrechnung“

Gefördert durch das Bundesministerium der Justiz und für Verbraucherschutz (BMJV) aufgrund eines Beschlusses des Deutschen Bundestages.

**Förderkennzeichen:** 28V2304A19, 28V2304B19, 28V2304C19, 28V2304D19

Laufzeit des Vorhabens: 01.01.2020 - 31.12.2021

**Download**: Der Bericht liegt in vor als 
 - [Druckfassung](Wissenschaftlicher%20Abschlussbericht_Druckfassung.pdf) (106 Seiten) sowie als
 - [Kurzfassung](Smart_HEC_Wissenschaftlicher_Abschlussbericht_Kurz%20(FKZ%2028V2304A19,%2028V2304B19,%2028V2304C19,%2028V2304D19).pdf) (10 Seiten).

---

## Zitation der Druckfassung des Abschlussberichts

Scheurer, Y., Albrecht, O., Diels, J., Münsch, M., Thorun, C., Otto, M., Balling, J., Binder, F., Philipp, J. N., Sachunsky, R., Niekler, A., Heyer, G.: Wissenschaftlicher Abschlussbericht Smart_HEC (Druckfassung). co2online gGmbH (2022).

```
@TechReport{Scheurer2022,
  author      = {Yvonne Scheurer and Oliver Albrecht and Jana Diels and Marlene Münsch and Christian Thorun and Markus Otto and Julian Balling and Frank Binder and J. Nathanael Philipp and Robert Sachunsky and Andreas Niekler and Gerhard Heyer},
  institution = {co2online gGmbH},
  title       = {Wissenschaftlicher Abschlussbericht Smart_HEC (Druckfassung)},
  year        = {2022},
  eprint = {https://git.informatik.uni-leipzig.de/smarthec/doc-abschlussbericht/-/blob/main/Wissenschaftlicher%20Abschlussbericht_Druckfassung.pdf},
  url         = {https://git.informatik.uni-leipzig.de/smarthec/doc-abschlussbericht},
}
```

---

## Projektdarstellungen im Web

Kurzdarstellungen des Projekts sind wie folgt verfügbar:

- co2online gGmbH:  
[Smarte Heizkostenabrechnung](https://www.co2online.de/smarte-heizkostenabrechnung/)
- ConPolicy - Institut für Verbraucherpolitik:  
[Der HeizCheck für alle mit KI](https://www.conpolicy.de/referenz/der-heizcheck-fuer-alle-mit-ki-empowerment-durch-eine-smarte-bewertung-der-heizkostenabrechnung/)  
(Hier sind die beiden Berichtsfassungen ebenfalls abrufbar.)
- Universität Leipzig, Lehrstuhl für Automatische Sprachverarbeitung:  
[SmartHEC - Der HeizCheck für alle mit KI](http://asv.informatik.uni-leipzig.de/de/projects/44)

---


## Weitere Publikationen

### Putting Users in the Loop: How User Research Can Guide AI Development for a Consumer-Oriented Self-service Portal

Paper presented at "AI-in-the-loop - Reconfiguring HCI for AI development",  
Panel at  *Culture and Computing - 10th International Conference, C&C 2022*,  
Held as Part of the *24th HCI International Conference, HCII 2022*.  
https://2022.hci.international/

Binder, F., Diels, J., Balling, J., Albrecht, O., Sachunsky, R., Philipp, J. N., Scheurer, Y., Münsch, M., Otto, M., Niekler, A., Heyer, G., Thorun, C.: Putting Users in the Loop: How User Research Can Guide AI Development for a Consumer-Oriented Self-service Portal. In: Rauterberg, M. (ed.) Culture and Computing. pp. 3–19. Springer International Publishing (2022). https://doi.org/10.1007/978-3-031-05434-1_1.

```
@InProceedings{Binder2022,
  author    = {Frank Binder and Jana Diels and Julian Balling and Oliver Albrecht and Robert Sachunsky and J. Nathanael Philipp and Yvonne Scheurer and Marlene Münsch and Markus Otto and Andreas Niekler and Gerhard Heyer and Christian Thorun},
  booktitle = {Culture and Computing},
  title     = {Putting Users in the Loop: How User Research Can Guide {AI} Development for a Consumer-Oriented Self-service Portal},
  year      = {2022},
  editor    = {Rauterberg, M.},
  number    = {13324},
  pages     = {3--19},
  publisher = {Springer International Publishing},
  volume    = {Lecture Notes in Computer Science},
  doi       = {10.1007/978-3-031-05434-1_1},
  url       = {https://link.springer.com/chapter/10.1007/978-3-031-05434-1_1},
}
```

Bibtex-Zitationen sind verfügbar in [smarthec_publikationen.bib](smarthec_publikationen.bib)